# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django.http import HttpResponse

from .db import getAlbums 



# Create your views here.

def index(request):
    allAlbums = getAlbums()
    html=''
    for album in allAlbums:
        url = '/music/' + str(album.id) + '/'
        html +='<a href="'+ url + '">'+album.album_title+'</a><br>' 
    return HttpResponse (html)

def detail(request, album_id):
    return  HttpResponse("<h2>Details for album id: " + album_id + "</h2>")
    
    