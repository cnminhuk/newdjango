# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.admin.sites import AdminSite
from .models import Album

# Register your models here.
admin.site.register(Album)
