# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Album(models.Model):
    artist = models.TextField(max_length=250)
    album_title = models.TextField(max_length=500)
    genre = models.TextField(max_length=100)
    album_logo = models.TextField(max_length=250)
    
    def __str__(self):
        return self.artist + ' - ' + self.album_title
    

class Song(models.Model):
    album = models.ForeignKey(Album,on_delete=models.CASCADE)
    file_type=models.TextField(max_length=10)
    song_title=models.TextField(max_length=250)

    def __str__(self):
        return self.album + ' - ' + self.song_title 
